package com.gitlab.adamkowski.pkpsimplified.query;

import junit.framework.TestCase;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TimetableParserTest extends TestCase {
    private final List<Connection> parsingResult;
    
    public TimetableParserTest() throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get("src/test/resources/server_response_example.txt"));
        String str = new String(bytes, Charset.defaultCharset());
        Document document = Jsoup.parse(str);
        parsingResult = new TimetableParser().parse(document);
    }

    public void testTimetableShouldContainFourRows() {
        Assert.assertEquals(parsingResult.size(), 4);
    }

    public void testParsedConnectionShouldHaveCorrectStations() {
        Connection connection = parsingResult.get(0);
        Assert.assertEquals("Szczecin Główny", connection.getStationFrom());
        Assert.assertEquals("Międzyzdroje", connection.getStationTo());
    }

    public void testPunctualTrainShouldHaveEmptyStringAsDelay() {
        Connection connection = parsingResult.get(1);
        Assert.assertEquals("", connection.getDelayFrom());
        Assert.assertEquals("", connection.getDelayTo());
    }

    public void testDelayedTrainShouldHaveInformationAboutExpectedDelayTime() {
        Connection connection = parsingResult.get(3);
        Assert.assertEquals("ok. +30 min.", connection.getDelayFrom());
        Assert.assertEquals("ok. +20 min.", connection.getDelayTo());
    }
}