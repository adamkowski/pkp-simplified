package com.gitlab.adamkowski.pkpsimplified.query;

import junit.framework.TestCase;

import org.junit.Assert;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;

public class TrainTimetableQueryTest extends TestCase {

    public void testQueryWithoutFieldsShouldThrowException() {
        TrainTimetableQuery query = new TrainTimetableQuery();
        Assert.assertThrows(MalformedURLException.class, query::generateQueryUrl);
    }

    public void testFilledQueryShouldReturnURLAddress() throws MalformedURLException {
        TrainTimetableQuery query = new TrainTimetableQuery();
        query.setStationFrom("X");
        query.setStationTo("X");
        query.setTravelDate("X");
        query.setTravelTime("X");
        String url = query.generateQueryUrl();
        Assert.assertEquals("", "https://rozklad-pkp.pl/pl/tp?REQ0JourneyStopsS0A=1&REQ0JourneyStopsS0G=X&REQ0JourneyStopsZ0A=1&REQ0JourneyStopsZ0G=X&date=X&dateStart=X&dateEnd=X&REQ0JourneyDate=X&time=X&REQ0JourneyTime=X&wDayExt0=Pn|Wt|%C5%9Ar|Cz|Pt|So|Nd&start=start&came_from_form=1", url);
    }

    public void testQueryShouldContainTravelDateFourTimes() throws MalformedURLException {
        TrainTimetableQuery query = new TrainTimetableQuery();
        query.setStationFrom("X");
        query.setStationTo("X");
        query.setTravelDate("07.03.21");
        query.setTravelTime("X");

        String url = query.generateQueryUrl();
        int occurrences = (url.length() - url.replace("07.03.21", "").length()) / 8;
        Assert.assertEquals(4, occurrences);
    }

    public void testQueryShouldContainTravelTimeTwoTimes() throws MalformedURLException {
        TrainTimetableQuery query = new TrainTimetableQuery();
        query.setStationFrom("X");
        query.setStationTo("X");
        query.setTravelDate("X");
        query.setTravelTime("23:15");

        String url = query.generateQueryUrl();
        int occurrences = (url.length() - url.replace("23:15", "").length()) / 5;
        Assert.assertEquals(2, occurrences);
    }

    public void testUrlCannotContainSpaces() throws MalformedURLException {
        TrainTimetableQuery query = new TrainTimetableQuery();
        query.setStationFrom("Xyz Abc");
        query.setStationTo("Qwe Rty");
        query.setTravelDate("X");
        query.setTravelTime("X");
        String url = query.generateQueryUrl();
        Assert.assertFalse(url.contains(" "));
    }

    public void testUrlCannotContainDiacriticalLetters() throws MalformedURLException {
        TrainTimetableQuery query = new TrainTimetableQuery();
        query.setStationFrom("ęśąćż");
        query.setStationTo("Pchnąć w tę łódź jeża lub ośm skrzyń fig");
        query.setTravelDate("X");
        query.setTravelTime("X");

        String url = query.generateQueryUrl();
        List<String> diacriticalLetters = Arrays.asList("ą", "ć", "ę", "ł", "ń", "ó", "ś", "ź", "ż");
        long occurrences = Arrays.stream(url.split(""))
                .filter(diacriticalLetters::contains)
                .count();
        Assert.assertEquals(0, occurrences);
    }
}