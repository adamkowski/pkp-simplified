package com.gitlab.adamkowski.pkpsimplified.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.gitlab.adamkowski.pkpsimplified.persistence.Station;
import com.gitlab.adamkowski.pkpsimplified.persistence.StationRepository;

import java.util.List;

public class StationViewModel extends AndroidViewModel {
    private final StationRepository stationRepository;
    private final LiveData<List<String>> stations;

    public StationViewModel(@NonNull Application application) {
        super(application);
        stationRepository = new StationRepository(application);
        stations = stationRepository.fetchAll();
    }

    public LiveData<List<String>> getStations() {
        return stations;
    }

    public void insert(Station... stations) {
        stationRepository.insert(stations);
    }

    public void delete(String station) {
        stationRepository.delete(station);
    }
}
