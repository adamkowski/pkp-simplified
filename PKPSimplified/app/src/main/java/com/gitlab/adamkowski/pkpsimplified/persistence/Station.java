package com.gitlab.adamkowski.pkpsimplified.persistence;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Station {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "station_name")
    private final String stationName;

    public Station(@NonNull String stationName) {
        this.stationName = stationName;
    }

    @NonNull
    public String getStationName() {
        return stationName;
    }
}
