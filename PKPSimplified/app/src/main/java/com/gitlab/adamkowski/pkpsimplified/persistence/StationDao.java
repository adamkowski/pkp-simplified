package com.gitlab.adamkowski.pkpsimplified.persistence;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StationDao {
    @Query("SELECT * FROM Station ORDER BY station_name ASC")
    LiveData<List<String>> fetchStations();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void saveStations(Station... stations);

    @Query("DELETE FROM Station WHERE station_name = :stationName")
    void deleteStation(String stationName);
}
