package com.gitlab.adamkowski.pkpsimplified.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.gitlab.adamkowski.pkpsimplified.databinding.ActivityConnectionDetailsBinding
import com.gitlab.adamkowski.pkpsimplified.query.Connection

class ConnectionDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityConnectionDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConnectionDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (intent.extras != null) {
            fillData(intent.getParcelableExtra("connection"))
        }
    }

    private fun fillData(connection: Connection?) {
        val d = connection?.date?.split(" ")
        binding.detailsDateFrom.text = d?.first() ?: ""
        binding.detailsDateTo.text = d?.last() ?: ""
        binding.detailsStationFrom.text = connection?.stationFrom
        binding.detailsStationTo.text = connection?.stationTo
        binding.detailsTimeFrom.text = connection?.timeFrom
        binding.detailsTimeTo.text = connection?.timeTo

        if (connection?.delayFrom?.isNotBlank() == true) {
            binding.detailsDelayFrom.text = connection.delayFrom
            binding.detailsDelayFrom.visibility = View.VISIBLE
        } else {
            binding.detailsDelayFrom.visibility = View.GONE
        }

        if (connection?.delayTo?.isNotBlank() == true) {
            binding.detailsDelayTo.text = connection.delayTo
            binding.detailsDelayTo.visibility = View.VISIBLE
        } else {
            binding.detailsDelayTo.visibility = View.GONE
        }

        binding.detailsDuration.text = connection?.duration
        binding.detailsChanges.text = connection?.changes
    }
}