package com.gitlab.adamkowski.pkpsimplified.adapter;

import com.gitlab.adamkowski.pkpsimplified.query.Connection;

public interface ConnectionsAdapterOnClickHandler {
    void onClick(Connection connection);
}
