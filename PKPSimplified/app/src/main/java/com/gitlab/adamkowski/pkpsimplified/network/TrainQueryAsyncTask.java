package com.gitlab.adamkowski.pkpsimplified.network;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gitlab.adamkowski.pkpsimplified.R;
import com.gitlab.adamkowski.pkpsimplified.adapter.ConnectionsAdapter;
import com.gitlab.adamkowski.pkpsimplified.query.Connection;
import com.gitlab.adamkowski.pkpsimplified.query.TimetableParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

public class TrainQueryAsyncTask extends AsyncTask<String, Void, Document> {
    private final WeakReference<Activity> weakActivity;
    private final ConnectionsAdapter connectionsAdapter;

    public TrainQueryAsyncTask(Activity myActivity, ConnectionsAdapter connectionsAdapter) {
        this.weakActivity = new WeakReference<>(myActivity);
        this.connectionsAdapter = connectionsAdapter;
    }

    @Override
    protected Document doInBackground(String... urls) {
        try {
            return Jsoup.connect(urls[0]).get();
        } catch (IOException e) {
            Log.d("TrainQueryAsyncTask", "An error occurred - probably no internet connection");
        }
        return null;
    }

    @Override
    protected void onPostExecute(Document document) {
        Activity activity = weakActivity.get();
        ProgressBar progressBar = activity.findViewById(R.id.pb_loading_timetable);
        LinearLayout layoutLoadingFailed = activity.findViewById(R.id.layout_loading_failed);
        Button refreshButton = activity.findViewById(R.id.button_refresh);
        TextView tvNoResults = activity.findViewById(R.id.tv_no_results);

        if (document == null) {
            progressBar.setVisibility(View.GONE);
            layoutLoadingFailed.setVisibility(View.VISIBLE);
            refreshButton.setVisibility(View.VISIBLE);
            return;
        }

        try {
            List<Connection> connections = new TimetableParser().parse(document);
            connectionsAdapter.setConnections(connections);
        } catch (IllegalArgumentException e) {
            progressBar.setVisibility(View.GONE);
            tvNoResults.setVisibility(View.VISIBLE);
        }
        progressBar.setVisibility(View.GONE);
    }
}
