package com.gitlab.adamkowski.pkpsimplified.query;

import android.util.Log;

import org.jsoup.internal.StringUtil;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Locale;

public class TrainTimetableQuery {
    private static final String URL_ENCODING = "UTF-8";

    private static final String URL_BASE = "https://rozklad-pkp.pl/pl";
    private static final String URL_STATIONS = "/tp?REQ0JourneyStopsS0A=1&REQ0JourneyStopsS0G=%s&REQ0JourneyStopsZ0A=1&REQ0JourneyStopsZ0G=%s";
    private static final String URL_DATE = "&date=%s&dateStart=%s&dateEnd=%s&REQ0JourneyDate=%s";
    private static final String URL_TIME = "&time=%s&REQ0JourneyTime=%s";
    private static final String URL_TERMINATE = "&wDayExt0=Pn|Wt|%C5%9Ar|Cz|Pt|So|Nd&start=start&came_from_form=1";

    private String stationFrom;
    private String stationTo;
    private String travelDate;
    private String travelTime;

    public TrainTimetableQuery() {
    }

    public void setStationFrom(String stationFrom) {
        try {
            this.stationFrom = URLEncoder.encode(stationFrom, URL_ENCODING);
        } catch (UnsupportedEncodingException ignored) {
            Log.e("TrainTimetableQuery", "Unsupported URL encoding");
        }
    }

    public void setStationTo(String stationTo) {
        try {
            this.stationTo = URLEncoder.encode(stationTo, URL_ENCODING);
        } catch (UnsupportedEncodingException ignored) {
            Log.e("TrainTimetableQuery", "Unsupported URL encoding");
        }
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String generateQueryUrl() throws MalformedURLException {
        if (StringUtil.isBlank(stationFrom)
                || StringUtil.isBlank(stationTo)
                || StringUtil.isBlank(travelDate)
                || StringUtil.isBlank(travelTime)) {
            throw new MalformedURLException("Query must contain all elements.");
        }
        return URL_BASE +
                String.format(Locale.getDefault(), URL_STATIONS, stationFrom, stationTo) +
                String.format(Locale.getDefault(), URL_DATE, travelDate, travelDate, travelDate, travelDate) +
                String.format(Locale.getDefault(), URL_TIME, travelTime, travelTime) +
                URL_TERMINATE;
    }
}
