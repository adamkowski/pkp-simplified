package com.gitlab.adamkowski.pkpsimplified.activity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.gitlab.adamkowski.pkpsimplified.R
import com.gitlab.adamkowski.pkpsimplified.databinding.ActivityMainBinding
import com.gitlab.adamkowski.pkpsimplified.persistence.Station
import com.gitlab.adamkowski.pkpsimplified.query.TrainTimetableQuery
import com.gitlab.adamkowski.pkpsimplified.viewmodel.StationViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import java.net.MalformedURLException
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var stationViewModel: StationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val viewModelProvider = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)
        )
        stationViewModel = viewModelProvider.get(StationViewModel::class.java)

        val query = TrainTimetableQuery()
        configureDropdownInputFields()
        configureDatePicker(query)
        configureTimePicker(query)

        binding.buttonSearchTrain.setOnClickListener {
            val validated = validateForm()
            if (validated) {
                saveStationHintsToDatabase()
                openResultsScreen(query)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.button_delete_stations) {
            displayStationsEditDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun displayStationsEditDialog() {
        val stations: MutableList<String> = ArrayList()
        stationViewModel.stations.observe(this, { collection: List<String>? ->
            stations.addAll(
                collection!!
            )
        })
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Usuwanie stacji")
        builder.setMultiChoiceItems(stations.toTypedArray(), null, null)
        builder.setPositiveButton("Usuń") { dialog: DialogInterface, _: Int ->
            val listView = (dialog as AlertDialog).listView
            for (item in stations.indices) {
                if (listView.isItemChecked(item)) {
                    stationViewModel.delete(stations[item])
                }
            }
        }
        builder.setNegativeButton("Anuluj", null)
        val dialog = builder.create()
        dialog.show()
    }

    private fun configureDropdownInputFields() {
        stationViewModel.stations.observe(this, { stations ->
            binding.inputDropdownStationFrom.setAdapter(
                ArrayAdapter(
                    this,
                    android.R.layout.simple_spinner_dropdown_item,
                    stations
                )
            )
            binding.inputDropdownStationTo.setAdapter(
                ArrayAdapter(
                    this,
                    android.R.layout.simple_spinner_dropdown_item,
                    stations
                )
            )
        })
    }

    private fun configureDatePicker(query: TrainTimetableQuery) {
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText(getString(R.string.select_travel_date))
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build()

        datePicker.addOnPositiveButtonClickListener {
            val date = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(it)
            binding.inputTravelDatePicker.setText(date)
            query.setTravelDate(date)
        }

        binding.inputTravelDatePicker.setOnClickListener {
            datePicker.show(supportFragmentManager, "datePicker")
        }
    }

    private fun configureTimePicker(query: TrainTimetableQuery) {
        val timePicker = MaterialTimePicker.Builder()
            .setTitleText(R.string.select_departure_time)
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .build()

        timePicker.addOnPositiveButtonClickListener {
            val hour = timePicker.hour.toString()
            val minute = timePicker.minute.toString()
            val time = "${hour.padStart(2, '0')}:${minute.padStart(2, '0')}"
            binding.inputTravelTimePicker.setText(time)
            query.setTravelTime(time)
        }

        binding.inputTravelTimePicker.setOnClickListener {
            timePicker.show(supportFragmentManager, "timePicker")
        }
    }

    private fun validateForm(): Boolean {
        return (validateStationFromInput()
                and validateStationToInput()
                and validateDateInput()
                and validateTimeInput())
    }

    private fun validateStationFromInput(): Boolean {
        if (binding.inputDropdownStationFrom.text.toString().isEmpty()) {
            binding.inputFieldStationFrom.error = "Należy wskazać stację początkową"
            return false
        }
        binding.inputFieldStationFrom.error = null
        binding.inputFieldStationFrom.isErrorEnabled = false
        return true
    }

    private fun validateStationToInput(): Boolean {
        if (binding.inputDropdownStationTo.text.toString().isEmpty()) {
            binding.inputFieldStationTo.error = "Należy wskazać stację końcową"
            return false
        }
        if (binding.inputDropdownStationTo.text.toString() == binding.inputDropdownStationFrom.text.toString()) {
            binding.inputFieldStationTo.error = "Stacja początkowa i końcowa muszą się różnić"
            return false
        }
        binding.inputFieldStationTo.error = null
        binding.inputFieldStationTo.isErrorEnabled = false
        return true
    }

    private fun validateDateInput(): Boolean {
        if (binding.inputTravelDatePicker.text.toString().isEmpty()) {
            binding.inputFieldTravelDate.error = "Należy wskazać datę podróży"
            return false
        }
        binding.inputFieldTravelDate.error = null
        binding.inputFieldTravelDate.isErrorEnabled = false
        return true
    }

    private fun validateTimeInput(): Boolean {
        if (binding.inputTravelTimePicker.text.toString().isEmpty()) {
            binding.inputFieldTravelTime.error = "Należy wskazać godzinę podróży"
            return false
        }
        binding.inputFieldTravelTime.error = null
        binding.inputFieldTravelTime.isErrorEnabled = false
        return true
    }

    private fun openResultsScreen(query: TrainTimetableQuery) {
        try {
            query.setStationFrom(binding.inputDropdownStationFrom.text.toString())
            query.setStationTo(binding.inputDropdownStationTo.text.toString())
            val intent = Intent(applicationContext, SearchResultsActivity::class.java)
            intent.putExtra("query_url", query.generateQueryUrl())
            startActivity(intent)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
    }

    private fun saveStationHintsToDatabase() {
        val stationFrom = Station(binding.inputDropdownStationFrom.text.toString())
        val stationTo = Station(binding.inputDropdownStationTo.text.toString())
        stationViewModel.insert(stationFrom, stationTo)
    }
}