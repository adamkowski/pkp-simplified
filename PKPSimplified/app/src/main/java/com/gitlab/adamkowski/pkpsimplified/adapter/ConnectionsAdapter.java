package com.gitlab.adamkowski.pkpsimplified.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.gitlab.adamkowski.pkpsimplified.R;
import com.gitlab.adamkowski.pkpsimplified.query.Connection;

import org.jsoup.internal.StringUtil;

import java.util.List;

import static android.view.View.OnClickListener;
import static androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class ConnectionsAdapter extends Adapter<ConnectionsAdapter.ConnectionsAdapterViewHolder> {
    private final ConnectionsAdapterOnClickHandler clickHandler;
    private List<Connection> connections;

    public ConnectionsAdapter(ConnectionsAdapterOnClickHandler clickHandler) {
        this.clickHandler = clickHandler;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ConnectionsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        int layoutIdForListItem = R.layout.connection_item;
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new ConnectionsAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConnectionsAdapterViewHolder holder, int position) {
        Connection connectionOnScreen = connections.get(position);
        holder.date.setText(connectionOnScreen.getDate());
        holder.stationFrom.setText(connectionOnScreen.getStationFrom());
        holder.timeFrom.setText(connectionOnScreen.getTimeFrom());
        holder.stationTo.setText(connectionOnScreen.getStationTo());
        holder.timeTo.setText(connectionOnScreen.getTimeTo());
        if (StringUtil.isBlank(connectionOnScreen.getDelayFrom())
                && StringUtil.isBlank(connectionOnScreen.getDelayTo())) {
            holder.delayInfo.setVisibility(View.GONE);
        } else {
            holder.delayInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (connections == null) {
            return 0;
        }
        return connections.size();
    }

    public class ConnectionsAdapterViewHolder extends ViewHolder implements OnClickListener {

        public final TextView date;
        public final TextView stationFrom;
        public final TextView stationTo;
        public final TextView timeFrom;
        public final TextView timeTo;
        public final TextView delayInfo;

        public ConnectionsAdapterViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            date = itemView.findViewById(R.id.tv_date);
            stationFrom = itemView.findViewById(R.id.tv_station_from);
            stationTo = itemView.findViewById(R.id.tv_station_to);
            timeFrom = itemView.findViewById(R.id.tv_time_from);
            timeTo = itemView.findViewById(R.id.tv_time_to);
            delayInfo = itemView.findViewById(R.id.delay_info);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            clickHandler.onClick(connections.get(adapterPosition));
        }
    }
}
