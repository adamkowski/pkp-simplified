package com.gitlab.adamkowski.pkpsimplified.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.adamkowski.pkpsimplified.adapter.ConnectionsAdapter
import com.gitlab.adamkowski.pkpsimplified.adapter.ConnectionsAdapterOnClickHandler
import com.gitlab.adamkowski.pkpsimplified.databinding.ActivitySearchResultsBinding
import com.gitlab.adamkowski.pkpsimplified.network.TrainQueryAsyncTask
import com.gitlab.adamkowski.pkpsimplified.query.Connection

class SearchResultsActivity : AppCompatActivity(), ConnectionsAdapterOnClickHandler {

    private lateinit var binding: ActivitySearchResultsBinding
    private val connectionsAdapter = ConnectionsAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchResultsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.pbLoadingTimetable.visibility = View.VISIBLE
        binding.layoutLoadingFailed.visibility = View.GONE
        binding.buttonRefresh.visibility = View.GONE
        binding.tvNoResults.visibility = View.GONE
        configureRecyclerView(binding.rvTrainInfoCard)

        val queryUrl = intent.extras!!.getString("query_url")
        Log.d("SearchResultsActivity", "queryUrl = $queryUrl")
        TrainQueryAsyncTask(this, connectionsAdapter).execute(queryUrl)
    }

    fun refresh(view: View) {
        recreate()
    }

    private fun configureRecyclerView(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = connectionsAdapter
    }

    override fun onClick(connection: Connection) {
        val intent = Intent(applicationContext, ConnectionDetailsActivity::class.java)
        intent.putExtra("connection", connection)
        startActivity(intent)
    }
}