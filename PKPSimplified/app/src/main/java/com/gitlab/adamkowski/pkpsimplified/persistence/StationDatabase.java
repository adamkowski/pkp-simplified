package com.gitlab.adamkowski.pkpsimplified.persistence;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Station.class}, version = 1, exportSchema = false)
public abstract class StationDatabase extends RoomDatabase {
    public abstract StationDao stationDao();

    private static volatile StationDatabase INSTANCE;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(1);

    public static StationDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (StationDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            StationDatabase.class,
                            "station_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
