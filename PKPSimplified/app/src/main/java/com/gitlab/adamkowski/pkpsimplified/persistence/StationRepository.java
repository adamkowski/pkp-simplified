package com.gitlab.adamkowski.pkpsimplified.persistence;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class StationRepository {
    private final StationDao stationDao;
    private final LiveData<List<String>> stations;

    public StationRepository(Application application) {
        StationDatabase database = StationDatabase.getDatabase(application);
        this.stationDao = database.stationDao();
        this.stations = stationDao.fetchStations();
    }

    public LiveData<List<String>> fetchAll() {
        return stations;
    }

    public void insert(Station... stations) {
        StationDatabase.databaseWriteExecutor.execute(() -> stationDao.saveStations(stations));
    }

    public void delete(String station) {
        StationDatabase.databaseWriteExecutor.execute(() -> stationDao.deleteStation(station));
    }
}
