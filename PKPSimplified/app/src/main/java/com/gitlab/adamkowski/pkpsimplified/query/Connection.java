package com.gitlab.adamkowski.pkpsimplified.query;

import android.os.Parcel;
import android.os.Parcelable;

public class Connection implements Parcelable {
    private String date;
    private String stationFrom;
    private String stationTo;
    private String timeFrom;
    private String timeTo;
    private String delayFrom;
    private String delayTo;
    private String duration;
    private String changes;

    public Connection() {
    }

    protected Connection(Parcel in) {
        date = in.readString();
        stationFrom = in.readString();
        stationTo = in.readString();
        timeFrom = in.readString();
        timeTo = in.readString();
        delayFrom = in.readString();
        delayTo = in.readString();
        duration = in.readString();
        changes = in.readString();
    }

    public static final Creator<Connection> CREATOR = new Creator<Connection>() {
        @Override
        public Connection createFromParcel(Parcel in) {
            return new Connection(in);
        }

        @Override
        public Connection[] newArray(int size) {
            return new Connection[size];
        }
    };

    public String getDate() {
        return date;
    }

    public Connection setDate(String date) {
        this.date = date;
        return this;
    }

    public String getStationFrom() {
        return stationFrom;
    }

    public Connection setStationFrom(String stationFrom) {
        this.stationFrom = stationFrom;
        return this;
    }

    public String getStationTo() {
        return stationTo;
    }

    public Connection setStationTo(String stationTo) {
        this.stationTo = stationTo;
        return this;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public Connection setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
        return this;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public Connection setTimeTo(String timeTo) {
        this.timeTo = timeTo;
        return this;
    }

    public String getDelayFrom() {
        return delayFrom;
    }

    public Connection setDelayFrom(String delayFrom) {
        this.delayFrom = delayFrom;
        return this;
    }

    public String getDelayTo() {
        return delayTo;
    }

    public Connection setDelayTo(String delayTo) {
        this.delayTo = delayTo;
        return this;
    }

    public String getDuration() {
        return duration;
    }

    public Connection setDuration(String duration) {
        this.duration = duration;
        return this;
    }

    public String getChanges() {
        return changes;
    }

    public Connection setChanges(String changes) {
        this.changes = changes;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(stationFrom);
        dest.writeString(stationTo);
        dest.writeString(timeFrom);
        dest.writeString(timeTo);
        dest.writeString(delayFrom);
        dest.writeString(delayTo);
        dest.writeString(duration);
        dest.writeString(changes);
    }
}
