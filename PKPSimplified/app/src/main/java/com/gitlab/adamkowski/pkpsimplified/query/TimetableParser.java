package com.gitlab.adamkowski.pkpsimplified.query;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class TimetableParser {
    public List<Connection> parse(Document document) {
        Elements rows = extractRows(document);
        if (rows.size() == 0) {
            throw new IllegalArgumentException("The timetable for the specified document cannot be found");
        }

        List<Connection> connections = new ArrayList<>(4);
        for (Element row : rows) {
            removeRedundantTags(row);
            connections.add(extractConnectionRow(row));
        }

        return connections;
    }

    private Elements extractRows(Document document) {
        Elements table = document.select("#wyniki");
        table.select("thead").remove();
        return table.select("tr");
    }

    private void removeRedundantTags(Element row) {
        row.select("wcag-r").remove();
        row.select("products-column").remove();
        row.select("upcase").remove();
        row.select("tabsy").remove();
    }

    private Connection extractConnectionRow(Element row) {
        return new Connection()
                .setStationFrom(extractStationFrom(row))
                .setStationTo(extractStationTo(row))
                .setDate(extractDate(row))
                .setTimeFrom(extractTimeFrom(row))
                .setDelayFrom(extractDelayFrom(row))
                .setTimeTo(extractTimeTo(row))
                .setDelayTo(extractDelayTo(row))
                .setDuration(extractDuration(row))
                .setChanges(extractChanges(row));
    }

    private String extractStationFrom(Element row) {
        return row.select("td").get(1).select("span").get(0).text();
    }

    private String extractStationTo(Element row) {
        return row.select("td").get(1).select("span").get(1).text();
    }

    private String extractDate(Element row) {
        return row.select("td").get(2).text();
    }

    private String extractTimeFrom(Element row) {
        return row.select("td").get(3).child(0).child(0).child(2).text();
    }

    private String extractDelayFrom(Element row) {
        Element delayFrom = row.select("td").get(3).child(0).child(1);
        if (isTrainDelayed(delayFrom)) {
            return delayFrom.select(".rtinfo").first().text();
        }
        return "";
    }

    private String extractTimeTo(Element row) {
        return row.select("td").get(3).child(1).child(0).child(2).text();
    }

    private String extractDelayTo(Element row) {
        Element delayTo = row.select("td").get(3).child(1).child(1);
        if (isTrainDelayed(delayTo)) {
            return delayTo.select(".rtinfo").first().text();
        }
        return "";
    }

    private boolean isTrainDelayed(Element possibleDelay) {
        return !possibleDelay.is("img")
                && possibleDelay.select(".rtinfo").first() != null;
    }

    private String extractDuration(Element row) {
        return row.select("td").get(4).text();
    }

    private String extractChanges(Element row) {
        return row.select("td").get(5).text();
    }
}
