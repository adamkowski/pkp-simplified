package com.gitlab.adamkowski.pkpsimplified.persistence;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class StationDaoTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private StationDao stationDao;
    private StationDatabase stationDatabase;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        stationDatabase = Room.inMemoryDatabaseBuilder(context, StationDatabase.class)
                .allowMainThreadQueries()
                .build();
        stationDao = stationDatabase.stationDao();
    }

    @After
    public void closeDb() {
        stationDatabase.close();
    }

    @Test
    public void getAllStations() throws InterruptedException {
        Station station1 = new Station("Abc");
        Station station2 = new Station("Def");

        stationDao.saveStations(station1, station2);
        List<String> stations = LiveDataTestUtil.getValue(stationDao.fetchStations());

        assertEquals(stations.get(0), station1.getStationName());
        assertEquals(stations.get(1), station2.getStationName());
    }

    @Test
    public void deleteStation() throws InterruptedException {
        stationDao.saveStations(new Station("Xyz"));
        stationDao.deleteStation("Xyz");

        List<String> stations = LiveDataTestUtil.getValue(stationDao.fetchStations());
        assertTrue(stations.isEmpty());
    }
}

class LiveDataTestUtil {
    public static <T> T getValue(final LiveData<T> liveData) throws InterruptedException {
        final Object[] data = new Object[1];
        final CountDownLatch latch = new CountDownLatch(1);
        Observer<T> observer = new Observer<T>() {
            @Override
            public void onChanged(@Nullable T o) {
                data[0] = o;
                latch.countDown();
                liveData.removeObserver(this);
            }
        };
        liveData.observeForever(observer);
        latch.await(2, TimeUnit.SECONDS);
        //noinspection unchecked
        return (T) data[0];
    }
}